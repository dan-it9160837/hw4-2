"use strict";

// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

// Конструкцію try...catch доречно використовувати коли працюємо з мережею, при роботі з об'єктами. 
// Парсинг даних - try...catch дозволяє виявити неправильний формат даних та обробити це.

// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price).
// Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

if (document.getElementById('root')) {
    const rootDiv = document.getElementById('root');
  
    const ol = document.createElement('ol');
  
    books.forEach(book => {
      if (book.hasOwnProperty('author') && book.hasOwnProperty('name') && book.hasOwnProperty('price')) {
        const li = document.createElement('li');
  
        li.textContent = `Author: ${book.author}, Name: ${book.name}, Price: ${book.price}`;
  
        ol.appendChild(li);
      } else {
        console.error(`Missing properties in book: ${JSON.stringify(book)}`);
      }
    });
  
    rootDiv.appendChild(ol);
  } else {
    console.error('Div with id "root" not found');
  }
